#include "touch.h"
#include <util/delay.h>

int touch_time_measure(pin_t *pin)
{
    *pin->ddr |= 1 << pin->pin_no;
    *pin->port &= ~(1 << pin->pin_no); // discharge the capacitance
    _delay_us(1);
    *pin->ddr &= ~(1 << pin->pin_no);
    int time_elapsed;
    for (time_elapsed = 0; !(*pin->pin & (1 << pin->pin_no)); time_elapsed++)
        ; // wait for C to charge

    return time_elapsed;
}

int touch_check(touch_sensor_t *sensor)
{

    int touch_time = touch_time_measure(&sensor->pin);

    sensor->max_time = MAX(sensor->max_time, touch_time);
    if (sensor->count++ == 30)
    {
        if (sensor->is_touched && sensor->max_time < sensor->treshold_low)
        {
            sensor->is_touched = 0;
        }
        if (!sensor->is_touched && sensor->max_time > sensor->treshold_high)
        {
            sensor->is_touched = 1;
        }
        sensor->max_time = 0;
        sensor->count = 0;
    }

    return sensor->is_touched;
}

static int find_max_time_position(touch_slider_t* slider){
    int i, max=0, maxi=0;
    for(i=0; i<SLIDER_SIZE; i++){
        if(slider->sensors[i].max_time > max){
            max = slider->sensors[i].max_time;
            maxi = i;
        }
    }
    return maxi;
}

int touch_check_slider(touch_slider_t* slider){
    int touched = 0;
    for(int i=0; i<SLIDER_SIZE; i++){
        if(touch_check(&slider->sensors[i])){
            touched = 1;
        }
    }
    if(touched){
        if(slider->position >= 0 && slider->position < SLIDER_SIZE){
            int new_max_pos = find_max_time_position(slider);
            if(new_max_pos != slider->position &&
                slider->sensors[new_max_pos].max_time > 2*slider->sensors[slider->position].max_time){
                slider->position = new_max_pos;
            }
        }else{
            slider->position = find_max_time_position(slider);
        }
    }else{
        slider->position = -1;
    }
    return slider->position;
}