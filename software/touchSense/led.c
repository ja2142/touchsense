#include <avr/io.h>

void led_init(){
    DDRC |= 1<<7 | 1<<6;
    DDRB |= 1<<6;
}

void led_on(int led){
    switch(led){
        case 0:
            PORTC |= 1<<7;
        break;
        case 1:
            PORTC |= 1<<6;
        break;
        case 2:
            PORTB |= 1<<6;
        break;
        default:;
    }
}

void led_off(int led){
    switch(led){
        case 0:
            PORTC &= ~(1<<7);
        break;
        case 1:
            PORTC &= ~(1<<6);
        break;
        case 2:
            PORTB &= ~(1<<6);
        break;
        default:;
    }
}

void led_toggle(int led){
    switch(led){
        case 0:
            PORTC ^= 1<<7;
        break;
        case 1:
            PORTC ^= 1<<6;
        break;
        case 2:
            PORTB ^= 1<<6;
        break;
        default:;
    }
}