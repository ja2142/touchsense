#ifndef _TOUCH_H_
#define _TOUCH_H_

	/* Includes: */
		#include <avr/io.h>
		#include <avr/wdt.h>
		#include <avr/power.h>
		#include <avr/interrupt.h>
		#include <string.h>
		#include <stdio.h>


		#define MIN( a, b ) ( (a < b) ? a : b )
		#define MAX( a, b ) ( (a > b) ? a : b )

		/** "constructor" for pin_t, example usage: pin_t pin_a0 = PIN(A, 0); */
		#define PIN(P, number) {&DDR##P, &PIN##P, &PORT##P, number}


	typedef struct{
		volatile uint8_t* ddr;  // direction register address
		volatile uint8_t* pin;  // input register address
		volatile uint8_t* port; // output register address
		uint8_t pin_no;			// pin number (0-7)
	}pin_t;

	typedef struct{
		pin_t pin;
		int max_time;
		int count;
		int treshold_low;
		int treshold_high;
		int is_touched;
	}touch_sensor_t;

	#define SLIDER_SIZE 6

	typedef struct{
		touch_sensor_t sensors[SLIDER_SIZE];
		int position;
	}touch_slider_t;

    int touch_check(touch_sensor_t* sensor);

	int touch_check_slider(touch_slider_t* slider);

#endif

