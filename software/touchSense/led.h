#ifndef _LED_H_
#define _LED_H_

    void led_init();

    void led_on(int led);

    void led_off(int led);

    void led_toggle(int led);

#endif